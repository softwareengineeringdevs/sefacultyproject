﻿using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using FacultyManagement.Models;
using FacultyManagement.Models.Users;

namespace FacultyManagement.Repositories
{
	public class AdminStaffRepository : IUserRepository<AdminStaff>
	{
		public void Add(AdminStaff user)
		{
			using (var context = new FacultyManagementContext())
			{
				context.AdminStaves.AddOrUpdate(user);
				context.SaveChanges();
			}
		}

		public void Delete(int id)
		{
			using (var context = new FacultyManagementContext())
			{
				var userToDelete = this.FindById(id);
				if (userToDelete == null)
				{
					throw new KeyNotFoundException("User not found!");
				}
				context.AdminStaves.Remove(userToDelete);
				context.SaveChanges();
			}
		}

		public void Update(AdminStaff newUser)
		{
			using (var context = new FacultyManagementContext())
			{
				context.AdminStaves.AddOrUpdate(newUser);
				context.SaveChanges();
			}
		}

		public AdminStaff FindById(int id)
		{
			using (var context = new FacultyManagementContext())
			{
				return context.AdminStaves.Find(id);
			}
		}

		public List<AdminStaff> GetAll()
		{
			using (var context = new FacultyManagementContext())
			{
				return context.AdminStaves.ToList();
			}
		}
	}
}