﻿using System.Collections.Generic;

namespace FacultyManagement.Repositories
{
	public interface IUserRepository<User>
	{
		void Add(User user);

		void Delete(int id);

		void Update(User newUser);

		User FindById(int id);

		List<User> GetAll();
	}
}
