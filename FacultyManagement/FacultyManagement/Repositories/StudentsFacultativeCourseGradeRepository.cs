﻿using System.Collections.Generic;
using System.Linq;
using FacultyManagement.Models.Courses;
using FacultyManagement.Models.StudentsGrade;
using FacultyManagement.Models;
using FacultyManagement.Models.Users;
using System.Data.Entity.Migrations;

namespace FacultyManagement.Repositories
{
    public class StudentsFacultativeCourseGradeRepository:IStudentsGradeRepository<StudentsFacultativeCourseGrade>
    {

        public double CourseAverage(int id)
        {
            using (var context = new FacultyManagementContext())
            {
                int[] average = new int[10000];
                int i = 0;
                foreach (StudentsFacultativeCourseGrade course in context.StudentsFacultativeCourseGrade.ToList())
                    if (course.FacultativeCourse_Id == id)
                    {
                        average[i] = course.Grade;
                        i++;
                    }
                return average.Average();

            }
        }



        public void Add(StudentsFacultativeCourseGrade studentGrade)
        {
            using (var context = new FacultyManagementContext())
            {
                context.StudentsFacultativeCourseGrade.AddOrUpdate(studentGrade);
                context.SaveChanges();
            }
        }

        public void Delete(int id)
        {
            using (var context = new FacultyManagementContext())
            {
                var userGrade = this.FindById(id);
                if (userGrade == null)
                {
                    throw new KeyNotFoundException("User not found!");
                }
                context.StudentsFacultativeCourseGrade.Remove(userGrade);
                context.SaveChanges();
            }
        }


        public void Update(StudentsFacultativeCourseGrade userGrade)
        {
            using (var context = new FacultyManagementContext())
            {
                context.StudentsFacultativeCourseGrade.AddOrUpdate(userGrade);
                context.SaveChanges();
            }
        }


        public StudentsFacultativeCourseGrade FindById(int id)
        {
            using (var context = new FacultyManagementContext())
            {
                return context.StudentsFacultativeCourseGrade.Find(id);
            }
        }

		public StudentsFacultativeCourseGrade FindWithoutID(int studID, int courseID) {
			using(var context =new FacultyManagementContext()){
				return context.StudentsFacultativeCourseGrade.Where(s=>s.FacultativeCourse_Id==courseID && s.StudentId==studID).FirstOrDefault();
			}
		}

        public List<ICourse> FindStudentCourse(int studentId)
        {
            List<ICourse> list = new List<ICourse>();
            using (var context = new FacultyManagementContext())
            {
                foreach (StudentsFacultativeCourseGrade studentGrade in context.StudentsFacultativeCourseGrade.ToList())
                {
                    if (studentGrade.StudentId == studentId)
                        list.Add(context.FacultativeCourses.Find(studentGrade.FacultativeCourse_Id));
                }
            }
            return list;
        }

        public List<Student> FindCourseStudents(int courseId)
        {
            List<Student> list = new List<Student>();
            using (var context = new FacultyManagementContext())
            {
                foreach (StudentsFacultativeCourseGrade studentGrade in context.StudentsFacultativeCourseGrade.ToList())
                {
                    if (studentGrade.FacultativeCourse_Id == courseId)
                        list.Add(context.Students.Find(studentGrade.StudentId));
                }
            }
            return list;
        }

        public List<StudentsFacultativeCourseGrade> GetAll()
        {
            using (var context = new FacultyManagementContext())
            {
                return context.StudentsFacultativeCourseGrade.ToList();
            }
        }

        public int GetStudentGrade(int studentId, int courseId)
        {
            using (var context = new FacultyManagementContext())
            {
                foreach (StudentsFacultativeCourseGrade studentGrade in context.StudentsFacultativeCourseGrade.ToList())
                    if (studentGrade.StudentId == studentId && studentGrade.FacultativeCourse_Id == courseId)
                        return studentGrade.Grade;
                return -1;
            }
        }
    }
}