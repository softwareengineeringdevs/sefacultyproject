﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FacultyManagement.Models;
using System.Data.Entity.Migrations;

namespace FacultyManagement.Repositories {
	public class GradesRepository {
		public void Add(Grade mark) {
			using (var context = new FacultyManagementContext()) {
				context.Grades.AddOrUpdate(mark);
				context.SaveChanges();
			}
		}
	}
}