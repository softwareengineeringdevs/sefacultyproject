﻿using System.ComponentModel.DataAnnotations;
using FacultyManagement.Models.YearOfStudy;

namespace FacultyManagement.ViewModel
{
	public class StudentViewModel
	{
		public int Id { get; set; }

		[Required]
		public string Name { get; set; }

		[Required]
		public string Username { get; set; }

		[DataType(DataType.Password)]
		public string Password { get; set; }

		public StudentsGroup Group { get; set; }
	}
}