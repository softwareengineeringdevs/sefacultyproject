﻿using System.Collections.Generic;

namespace FacultyManagement.ViewModel
{
	public class StudentIndexViewModel
	{
		public List<StudentViewModel> Students { get; set; }
	}
}