﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FacultyManagement.ViewModel {
	public class OptCourseViewModel {
		public int CourseID {get;set;}

		public String CourseName { get; set; }

		public int MaxStuds { get; set; }

		public int CurrentStuds { get; set; }
	}
}