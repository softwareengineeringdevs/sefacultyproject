﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FacultyManagement.Models.Faculty;
using FacultyManagement.Models.Courses;

namespace FacultyManagement.ViewModel
{
	public class TeacherViewModel
	{
		public int Id { get; set; }

		[Required]
		public string Name { get; set; }

		[Required]
		public string Username { get; set; }

		[DataType(DataType.Password)]
		public string Password { get; set; }

		public Department Department { get; set; }

        public List<ICourse> Courses { get; set; }
	}
}