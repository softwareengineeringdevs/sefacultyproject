﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FacultyManagement.ViewModel {
	public enum CourseType{Mandatory,Facultative,Optional};

	public class CourseGradeViewModel {
		public int Id { set; get; }

		public int StudentId { set; get; }

		public int Grade { set; get; }

		public int Course_Id { set; get; }

		public String CourseName { set; get; }

		public String StudentName { set; get; }

		public CourseType Type { get; set; }
	}
}