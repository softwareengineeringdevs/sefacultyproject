﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FacultyManagement.ViewModel
{
	public class ChangePasswordViewModel
	{
		[Required]
		public string Username { get; set; }

		[Required(ErrorMessage = "Reset code not set")]
		public string ResetCode { get; set; }

		[Required(ErrorMessage = "Password not set")]
		[DataType(DataType.Password)]
		public string NewPassword { get; set; }

		public UserType TypeOfUser { get; set; }
	}
}