﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace FacultyManagement.Models.Users
{
	public interface IUser
	{
		[Key]
		int Id { get; set; }

		[Required]
		string Name { get; set; }
		
		[Required]
		string Username { get; set; }
		
		[PasswordPropertyText]
		[Required]
		string Password { get; set; }
	}
}
