﻿namespace FacultyManagement.Models.Users
{
	public class AdminStaff : IUser
	{
		public int Id { get; set; }

		public string Name { get; set; }

		public string Username { get; set; }

		public string Password { get; set; }
	}
}