﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FacultyManagement.Models.Courses;

namespace FacultyManagement.Models.YearOfStudy
{
	public class Semester
	{
		[Key]
		public int Id { get; set; }

		[Required]
		public int Number { get; set; }

		public virtual ICollection<ICourse> Courses { get; set; }

		public virtual ICollection<YearOfStudy> YearsOfStudy { get; set; }
	}
}