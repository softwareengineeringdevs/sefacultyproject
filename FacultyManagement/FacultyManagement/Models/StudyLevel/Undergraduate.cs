﻿using System.Collections.Generic;
using FacultyManagement.Models.Faculty;

namespace FacultyManagement.Models.StudyLevel
{
	public class Undergraduate : IStudyLevel
	{
		public int Id { get; set; }

		public string Name { get; set; }

		public virtual ICollection<Specialisation> Specialisations { get; set; }

		public virtual Department Department { get; set; }
	}
}