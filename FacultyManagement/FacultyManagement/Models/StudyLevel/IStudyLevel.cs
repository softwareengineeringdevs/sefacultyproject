﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using FacultyManagement.Models.Faculty;

namespace FacultyManagement.Models.StudyLevel
{
	public interface IStudyLevel
	{
		[Key]
		int Id { get; set; }

		[Required]
		string Name { get; set; }

		ICollection<Specialisation> Specialisations { get; set; }

		Department Department { get; set; }
	}
}
