﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FacultyManagement.Models {
	public class Grade {
		public int ID { get; set; }

		public int Mark { get; set; }

		public int StudentID { get; set; }

		public int? MandatoryID { get; set; }

		public int? OptionalID { get; set; }

		public int? FacultativeID { get; set; }
	}
}