﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FacultyManagement.Models.StudentsGrade;

namespace FacultyManagement.Models.StudentsGrade
{
    public class StudentsOptionalCourseGrade:IStudentsGrade
    {
        public int Id { set; get; }

        public int StudentId { set; get; }

        public int Grade { set; get; }

        public int OptionalCourse_Id { set; get; }
    }
}