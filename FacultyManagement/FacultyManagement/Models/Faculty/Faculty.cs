﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace FacultyManagement.Models.Faculty
{
	public class Faculty
	{
		[Key]
		public int Id { get; set; }
		
		[Required]
		public string Name { get; set; }

		public virtual ICollection<Department> Departments { get; set; }
	}
}