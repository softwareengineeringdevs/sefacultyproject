﻿using Autofac;
using FacultyManagement.Models.Courses;
using FacultyManagement.Models.StudentsGrade;
using FacultyManagement.Models.Users;
using FacultyManagement.Repositories;

namespace FacultyManagement.Bootstrap
{
	public class RepositoryRegistrar
	{
		public static void RegisterWith(ContainerBuilder builder)
		{
            builder.RegisterType<StudentsFacultativeCourseGradeRepository>().As<IStudentsGradeRepository<StudentsFacultativeCourseGrade>>();
            builder.RegisterType<StudentsMandatoryCourseGradeRepository>().As<IStudentsGradeRepository<StudentsMandatoryCourseGrade>>();
            builder.RegisterType<StudentsOptionalCourseGradeRepository>().As<IStudentsGradeRepository<StudentsOptionalCourseGrade>>();
			builder.RegisterType<TeacherRepository>().As<IUserRepository<Teacher>>();
			builder.RegisterType<StudentRepository>().As<IUserRepository<Student>>();
			builder.RegisterType<CODRepository>().As<IUserRepository<ChiefOfDepartment>>();
			builder.RegisterType<AdminStaffRepository>().As<IUserRepository<AdminStaff>>();
			builder.RegisterType<MandatoryCourseRepository>().As<ICourseRepository<MandatoryCourse>>();
			builder.RegisterType<OptionalCoursesRepository>().As<ICourseRepository<OptionalCourse>>();
			builder.RegisterType<FacultativeCoursesRepository>().As<ICourseRepository<FacultativeCourse>>();
			builder.RegisterType<StudentsFacultativeCourseGradeRepository>().As<IStudentsGradeRepository<StudentsFacultativeCourseGrade>>();
		}
	}
}