﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using FacultyManagement.Controllers;
using FacultyManagement.ViewModel;
using FacultyManagement.Models.YearOfStudy;
using FacultyManagement.Models.StudyLevel;
using FacultyManagement.Models.Faculty;
using FacultyManagement.Models.Courses;

namespace FacultyManagement.Views
{
    public partial class ChiefOfDepartment : System.Web.UI.Page
    {
        private ViewHomeController homeCtrl = new ViewHomeController();
        private ViewChiefOfDepartmentController codCtrl = new ViewChiefOfDepartmentController();
        protected void Page_Load(object sender, EventArgs e)
        {
			if (Session["New"] != null) {
				string Type = (string)Session["New"];
				Type = Type.Split('.')[0];
				if (Type == "ChiefOfDepartment") {
					LoadAllCourses();
				}
				else {
					Response.Redirect(Type + ".aspx");
				}
			}
			else {
				Response.Redirect("Login.aspx");
			}
        }

		protected void optCourses_RowCommand(object sender, GridViewCommandEventArgs e) {
			if (e.CommandName == "RemoveCourse") {
				int index = Convert.ToInt32(e.CommandArgument);
				GridViewRow row = optCourses.Rows[index];
				int ad=Convert.ToInt32(row.Cells[0].Text);
				codCtrl.DeleteOptionalCourse(ad);
			}
			if(e.CommandName=="DetailCourse"){
				//REDIRECT THE USER TO VIEW THE COURSE DETAILS
			}
			if (e.CommandName == "EditCourse") {
				//REDIRECT THE USER TO EDIT THE COURSE DETAILS
			}
		}

		protected void LoadAllCourses() {
			List<OptCourseViewModel> list= codCtrl.GetOptionalCourses();
			DataTable dt = new DataTable();
			dt.Columns.Add(new DataColumn("CourseID", typeof(String)));
			dt.Columns.Add(new DataColumn("CourseName", typeof(String)));
			dt.Columns.Add(new DataColumn("Fill Factor", typeof(String)));
			dt.Columns.Add(new DataColumn("Edit", typeof(Button)));
			dt.Columns.Add(new DataColumn("Details", typeof(Button)));
			dt.Columns.Add(new DataColumn("Remove", typeof(Button)));
			optCourses.DataSource = dt;
			foreach (OptCourseViewModel oCourse in list) {
				DataRow dr = dt.NewRow();
				dr["CourseID"] = oCourse.CourseID.ToString();
				dr["CourseName"] = oCourse.CourseName;
				dr["Fill Factor"] = oCourse.CurrentStuds+" / "+oCourse.MaxStuds;
				dt.Rows.Add(dr);
			}
			//Daca lista e goala afiseaza mesaj de eroare. daca tabelul e gol, atunci el nu mai e afisat
			optCourses.DataBind();
		}

        protected void ButtonAdd_Click(object sender, EventArgs e)
        {
            TeacherViewModel var = new TeacherViewModel();

            var.Name = TeacherAddName.Text;
            var.Department = new Department();
            var.Department.Name = TeacherAddDep.Text;
            var.Username = TeacherAddUsername.Text;
            var.Password = TeacherAddPassword.Text;

            if (TeacherAddPassword.Text == TeacherAddPassword.Text && TeacherAddPassword.Text != string.Empty)
            {
                if (codCtrl.Create(var))
                {
                    LabelAddInteract.Text = "Teacherul a fost adaugat";
                }
                else
                {
                    LabelAddInteract.Text = "Eroare, daca persista contactati administratorul";
                }
            }
            else
            {
                LabelAddInteract.Text = "Parolele nu corespund";
            }
        }

        protected void ButtonEdit_Click(object sender, EventArgs e)
        {
            TeacherViewModel var = new TeacherViewModel();

            string username = TeacherEditUsername.Text;

            var.Name = TeacherEditNewName.Text;
            var.Department = new Department();
            var.Department.Name = TeacherEditNewDep.Text;
            var.Username = TeacherEditNewUsername.Text;

            if (codCtrl.Edit(username, var))
            {
                LabelEditInteract.Text = "Teacherul a fost modificat";
            }
            else
            {
                LabelEditInteract.Text = "Eroare, verifica daca datele sunt valide - daca persista contactati administratorul";
            }
        }

        protected void ButtonDelete_Click(object sender, EventArgs e)
        {
            if (TeacherDeleteUsername.Text == TeacherDeleteConfirmUsername.Text)
            {
                if (codCtrl.Delete(TeacherDeleteUsername.Text))
                {
                    LabelDeleteInteract.Text = "Userul cu idul " + TeacherDeleteUsername.Text + " a fost sters";
                }
                else
                {
                    LabelDeleteInteract.Text = "Userul cu idul " + TeacherDeleteUsername.Text + " nu a fost sters, probabil ca nu exista";
                }
            }
            else
            {
                LabelDeleteInteract.Text = "Idurile introduse nu corespund";
            }
        }

        protected void ButtonDetails_Click(object sender, EventArgs e)
        {
            TeacherViewModel var = new TeacherViewModel();

            var = codCtrl.Details(TeacherDetailsUsername.Text);

            if (var != null)
            {
                TeacherDetailsReadUser.Text = var.Username;
                TeacherDetailsReadName.Text = var.Name;
                TeacherDetailsReadDepName.Text = var.Department.Name;

            }
        }

        protected void ButtonSignout_Click(object sender, EventArgs e)
        {
            Session.Remove("New");
            Session.RemoveAll();
            Response.Redirect("Login.aspx");
        }

        protected void ButtonBest_Click(object sender, EventArgs e)
        {
            TeacherViewModel teacher = codCtrl.TeacherStatistics(1);
            TextBox1.Text = teacher.Name;
            TextBox2.Text = teacher.Username;
            TextBox3.Text = teacher.Department.Name;
        }

        protected void ButtonWorst_Click(object sender, EventArgs e)
        {
            TeacherViewModel teacher = codCtrl.TeacherStatistics(2);
            TextBox1.Text = teacher.Name;
            TextBox2.Text = teacher.Username;
            TextBox3.Text = teacher.Department.Name;
        }
    }
}