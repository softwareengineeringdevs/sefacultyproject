﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdministrativeStaff.aspx.cs" Inherits="FacultyManagement.Views.AdministrativeStaff" %>


<!DOCTYPE html>
<html>

<head>
	<script type="text/javascript" src="resources/jq.js"></script>
	<script type="text/javascript" src="resources/parallax.min.js"></script>
	<script type="text/javascript" src="resources/AdministrativeStaff.js"></script>
	<link rel="Stylesheet" type="text/css" href="resources/C5.css">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<title>Administrative Staff</title>
	
	<script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-34546066-1']);
	  _gaq.push(['_trackPageview']);

	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>
	
</head>


<body class="bc">
    
    

    <form runat="server">

     <asp:ScriptManager ID="ScriptManager1" runat="server">
     </asp:ScriptManager>
    <a><img src="resources/Logo.png"/></a>
    <header>
        <a href="" class="toggle">≡</a>
        <div class="menu">
            <ul>
                <a id="CreateControl"><li>Create</li></a>
                <a id="DeleteControl"><li>Delete</li></a>
                <a id="DetailsControl" ><li>Details</li></a>
                <a id="EditControl" ><li>Edit</li></a>
                <a id="YearStatisticControl" ><li>Year</li></a>
                <a id="GroupStatisticControl" ><li>Group</li></a>
            </ul>
        </div>
        <asp:Button ID="ButtonSignout" type = "button" class="singout" runat="server" Text="SIGN OUT" OnClick="ButtonSignout_Click" />
    </header>
    
	
    <div id="index" class="container">
        <asp:Label ID="LabelHomePage" runat="server" class="edit_lable" Text="Home Page ADMIN"></asp:Label>
        <hr class="first_line">
    </div>
	

    <div id="add_user" class="container">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <label class="edit_lable">ADD STUDENT</label>
                    <hr class="first_line">
                    <div class="Edit">
                        <asp:TextBox ID="StudentAddName" runat="server" type="text" placeholder="Student Name" name="name"></asp:TextBox>
                        <asp:TextBox ID="StudentAddGroup" type="text" placeholder="Student Group" name="group" runat="server"></asp:TextBox>
                        <asp:TextBox ID="StudentAddUsername" type="text" placeholder="Username" name="user" runat="server"></asp:TextBox>
                        <asp:TextBox ID="StudentAddPassword" type="password" placeholder="Password" name="password" runat="server"></asp:TextBox>
                        <asp:TextBox ID="StudentAddConfirmPassword" type="password" placeholder="Confirm Password" name="confirm" runat="server"></asp:TextBox>
                        <input id="ButtonAdd" runat="server" type="button" value="ADD" onserverclick="ButtonAdd_Click"/>
                    </div>
                    </br>
                </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div id="edit_user" class="container">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    
                    <label class="edit_lable">EDIT STUDENT</label>
                    <hr class="first_line">
                    <div class="Edit">
            
                        <asp:TextBox ID="StudentEditUsername" runat="server" type="text" placeholder="Student Username" name="name"></asp:TextBox>
                        <asp:TextBox ID="StudentEditNewName" runat="server" type="text" placeholder="New Student Name" name="name"></asp:TextBox>
                        <asp:TextBox ID="StudentEditNewGroup" runat="server" type="text" placeholder="New Student Group" name="group"></asp:TextBox>
                        <asp:TextBox ID="StudentEditNewUsername" runat="server" type="text" placeholder="New Username" name="user"></asp:TextBox>
                        <input id="ButtonEdit" runat="server" type="button" value="EDIT" onserverclick="ButtonEdit_Click" />
                
                    </div>
                    </br>
                </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div id="delete_user" class="container">
        <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                <ContentTemplate>
                    <label class="edit_lable">DELETE STUDENT</label>
                    <hr class="first_line">
                    <div class="Edit">
                        <asp:TextBox ID="StudentDeleteUsername" runat="server" type="text" placeholder="Student Id" name="user"></asp:TextBox>
                        <asp:TextBox ID="StudentDeleteConfirmUsername" runat="server" type="text" placeholder="Confirm Student Id" name="user"></asp:TextBox>
                        <input id="ButtonDelete" runat="server" type="button" value="DELETE" onserverclick="ButtonDelete_Click"/>
                    </div>
                    </br>
                </ContentTemplate>
        </asp:UpdatePanel>
    </div>
		
    <div id="user_details" class="container">

        <asp:UpdatePanel ID="UpdatePanel4" runat="server">
                <ContentTemplate>
                    <label class="edit_lable">STUDENT DETAILS</label>
                    <hr class="first_line">
                    
                    <div class="Edit">
		                <asp:TextBox ID="StudentDetailsUsername" runat="server" type="text" placeholder="Username" name="user"></asp:TextBox>
                        <input id="ButtonDetails" runat="server" type="button" value="DETAILS" onserverclick="ButtonDetails_Click"/>
                    </div>
        
                   </br>
                    <div class="Edit">
                        <asp:TextBox ID="StudentDetailsReadUser" runat="server" type="text" placeholder="Username" name="user"></asp:TextBox>
                        <asp:TextBox ID="StudentDetailsReadName" runat="server" type="text" placeholder="Name" name="user"></asp:TextBox>
                        <asp:TextBox ID="StudentDetailsReadGroupName" runat="server" type="text" placeholder="Group" name="user"></asp:TextBox>
                    </div>

                </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div id="group_statistic" class="container">

        <asp:UpdatePanel ID="UpdatePanel5" runat="server">
                <ContentTemplate>
                    <label class="edit_lable">GROUP STATISTIC</label>
                    <hr class="first_line">
                    <div class="Edit">
		                <asp:TextBox ID="TextBoxGroupStatistic" runat="server" type="text" placeholder="Group" name="user"></asp:TextBox>
                        <input id="ButtonGS" runat="server" type="button" value="SEE STATISTIC" onserverclick="GS_Click"/>
                    </div>
        
                    </br>

                    <asp:Table ID="GradesG" runat="server" CssClass="table-fill">
                        <asp:TableHeaderRow CssClass="tr">
                            <asp:TableHeaderCell runat="server" CssClass="text-left th" Text="Name"/>
                            <asp:TableHeaderCell runat="server" CssClass="text-left th" Text="Averange"/>
                        </asp:TableHeaderRow>
                    </asp:Table>

                </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    <div id="year_statistic" class="container">
        <asp:UpdatePanel ID="UpdatePanel6" runat="server">
                <ContentTemplate>
                    <label class="edit_lable">YEAR STATISTIC</label>
                    <hr class="first_line">
                    <div class="Edit">
		                <asp:TextBox ID="TextBoxYearStatistic" runat="server" type="text" placeholder="Year" name="user"></asp:TextBox>
                        <input id="ButtonYS" runat="server" type="button" value="SEE STATISTIC" onserverclick="YS_Click"/>
                    </div>
        
                    <asp:Table ID="GradesY" runat="server" CssClass="table-fill">
                        <asp:TableHeaderRow CssClass="tr">
                            <asp:TableHeaderCell runat="server" CssClass="text-left th" Text="Name"/>
                            <asp:TableHeaderCell runat="server" CssClass="text-left th" Text="Averange"/>
                        </asp:TableHeaderRow>
                    </asp:Table>
                </ContentTemplate>
        </asp:UpdatePanel>
    </div>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>   
    <script>
        (function(){
            var body = $('body');
            $('.toggle').bind('click', function(){
                body.toggleClass('menu-open');
                return false;
            });
        })();
    </script>
       </form>
</body>

</html>



