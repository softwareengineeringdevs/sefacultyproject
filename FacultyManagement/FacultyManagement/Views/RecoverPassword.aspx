﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="RecoverPassword.aspx.cs" Inherits="FacultyManagement.Views.RecoverPassword" %>

<!DOCTYPE html>
<html>

<head>
  <link rel="stylesheet" type="text/css" href="resources/C2.css">
  <title>Recover</title>
</head>


<body runat="server">

    <img src="resources/Logo.png" id="logo">
    

	
    <h1 id="First_Title">Recover</h1>
    <h1 id="Second_Titile">Change</h1>

	
	<form id="form" runat="server">
			
	    <div class="F1">
            <asp:TextBox ID="Username_TextBox" runat="server" type="Username" name="User" Font-Names="Arial" Font-Size="Medium" placeholder="Username"></asp:TextBox>
            <asp:TextBox ID="Email_TextBox" runat="server" type="Username" name="Email" Font-Names="Arial" Font-Size="Medium" placeholder="Email"></asp:TextBox>
            <asp:DropDownList ID="Type_Select" runat="server">
                <asp:ListItem>Student</asp:ListItem>
                <asp:ListItem>Teacher</asp:ListItem>
                <asp:ListItem>AdministrativeStaff</asp:ListItem>
                <asp:ListItem>ChiefOfDepartment</asp:ListItem>
            </asp:DropDownList>
            <asp:Button ID="RecoverButton" runat="server" class="submit-button" type="submit" Text="Submit" OnClick="RecoverButton_Click"/>
        </div>

        <div class="F2">
            <asp:TextBox ID="TextBoxUsername" runat="server" type="Username" name="User" Font-Names="Arial" Font-Size="Medium" placeholder="Username"></asp:TextBox>
            <asp:TextBox ID="TextBoxResetCode" runat="server" type="Username" name="User" Font-Names="Arial" Font-Size="Medium" placeholder="Code"></asp:TextBox>
            <asp:TextBox ID="TextBoxPassword" runat="server" type="Password" name="User" Font-Names="Arial" Font-Size="Medium" placeholder="Password"></asp:TextBox>
            <asp:TextBox ID="TextBoxConfirmPassword" runat="server" type="Password" name="User" Font-Names="Arial" Font-Size="Medium" placeholder="Confirm Password"></asp:TextBox>
            <asp:DropDownList ID="DropDownList1" runat="server">
                <asp:ListItem>Student</asp:ListItem>
                <asp:ListItem>Teacher</asp:ListItem>
                <asp:ListItem>AdministrativeStaff</asp:ListItem>
                <asp:ListItem>ChiefOfDepartment</asp:ListItem>
            </asp:DropDownList>
            <asp:Button ID="ChangeButton" runat="server" class="ChangeButton" type="submit" Text="Change" OnClick="ChangeButton_Click"/>
        </div>
	</form>
	
\

    <hr class="verticalLine"/>
    

</body>

</html>
