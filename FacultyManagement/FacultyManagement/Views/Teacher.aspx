﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Teacher.aspx.cs" Inherits="FacultyManagement.Views.Teacher" %>

<!DOCTYPE html>

<html>

<head>
<%--	<script type="text/javascript" src="resources/jq.js"></script>
	<script type="text/javascript" src="resources/parallax.min.js"></script>
	<script type="text/javascript" src="resources/Teacher.js"></script>--%>
	<link rel="Stylesheet" type="text/css" href="resources/C3.css">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<title>Teacher</title>
	
<%--	<script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-34546066-1']);
	  _gaq.push(['_trackPageview']);

	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>
	--%>
</head>


<body class="bc">
    
    <form runat="server">

     <asp:ScriptManager ID="ScriptManager1" runat="server">
     </asp:ScriptManager>

    <a><img src="resources/Logo.png" style="display: block; margin-left: auto;margin-right: auto;margin-top: -170px;width: 311px;height: 303px;"/></a>
    <header>
        <a href="" class="toggle">≡</a>
        <div class="menu">
            <ul>
                <a id="AssignMarksControl"><li>Assign M</li></a>
            </ul>
        </div>
        <asp:Button ID="ButtonSignout" type = "button" class="singout" runat="server" Text="SIGN OUT" OnClick="ButtonSignout_Click" />
    </header>
    

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <label class="edit_lable">ASSIGN MARKS</label>
                <hr class="first_line">
                <div class="Edit">
                    <asp:DropDownList runat="server" ID="courseOption"/>
                    <input id="ButtonAssignMarks" runat="server" type="button" value="AssignMarks" onserverclick="ButtonAssignMarks_Click" />
                </div>
        
				<asp:GridView ID="courseMarks" runat="server" CssClass="table-fill" AutoGenerateColumns="false">
                    <Columns>
                        <asp:BoundField DataField="CourseID" HeaderText="Course ID"/>
                        <asp:BoundField DataField="CourseName" HeaderText="Course Name"/>
                        <asp:BoundField DataField="CourseType" HeaderText="Course Type"/>
                        <asp:BoundField DataField="StudentID" HeaderText="Student ID"/>
                        <asp:BoundField DataField="StudentName" HeaderText="Student Name"/>
		                <asp:TemplateField HeaderText="Grade">
                        <ItemTemplate>
                            <asp:TextBox ID="grade" TextMode="Number" MaxLength="2" runat="server"></asp:TextBox>
                        </ItemTemplate>
                            </asp:TemplateField>
		                <asp:TemplateField HeaderText="Confirm?">
                            <ItemTemplate>
                                <asp:CheckBox ID="confirm" runat="server"></asp:CheckBox>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>

                    <RowStyle CssClass="tr" />
                    <HeaderStyle CssClass="table-hover" />
                </asp:GridView>
            </ContentTemplate>
    </asp:UpdatePanel>

    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>   
    <script>
        (function () {
            var body = $('body');
            $('.toggle').bind('click', function () {
                body.toggleClass('menu-open');
                return false;
            });
        })();
    </script>
       </form>
</body>

</html>