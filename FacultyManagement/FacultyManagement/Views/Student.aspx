﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Student.aspx.cs" Inherits="FacultyManagement.Views.Student" %>

<!DOCTYPE html>

<html>

<head>
	<script type="text/javascript" src="resources/jq.js"></script>
	<script type="text/javascript" src="resources/parallax.min.js"></script>
	<script type="text/javascript" src="resources/Student.js"></script>
	<link rel="Stylesheet" type="text/css" href="resources/C4.css">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<title>Student</title>
	
	<script type="text/javascript">

	  var _gaq = _gaq || [];
	  _gaq.push(['_setAccount', 'UA-34546066-1']);
	  _gaq.push(['_trackPageview']);

	  (function() {
	    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
	    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
	    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
	  })();

	</script>
	
</head>


<body class="bc">
    
    
    <form runat="server">
     <asp:ScriptManager ID="ScriptManager1" runat="server">
     </asp:ScriptManager>
    
       <a><img src="resources/Logo.png"/></a>
        <header>
            <a href="" class="toggle">≡</a>
            <div class="menu">
                <%--AVEM ASA:BUTOANELE DIN MENUIU IS LEGATE DE FUNCTIILE DIN STUDENT.ASPX.CS--%>
                <ul>
                    <asp:LinkButton runat="server"  CssClass="links" OnClick="ButtonCourses_Click" Text="Courses"/>
                    <asp:LinkButton runat="server"  CssClass="links" Text="Settings"/>
                    <asp:LinkButton runat="server"  CssClass="links" Text="Export"/>
                </ul>
            </div>
            <asp:Button ID="ButtonSignout" class="singout" runat="server" Text="SIGN OUT" OnClick="ButtonSignout_Click" />
        </header>
    
        <%--HTML VERSION--%>
       <%-- <table class="table-fill">
            <thead>
                <tr>
                    <th class="text-left">Course</th>
                    <th class="text-left">Type</th>
                    <th class="text-left">Credits</th>
                    <th class="text-left">Mark</th>
                    <th class="text-left">Select</th>
                </tr>
            </thead>
    
            <tbody class="table-hover">
                <tr>
                    <td class="text-left">Arhitecura Calculatoarelor</td>
                    <td class="text-left">Mendatory</td>
                    <td class="text-left">6</td>
                    <td class="text-left">10</td>
                    <td class="text-left"><input type="checkbox"></td>
                </tr>
                
                <tr>
                    <td class="text-left">Aspect oriented programming</td>
                    <td class="text-left">Optional</td>
                    <td class="text-left">3</td>
                    <td class="text-left">10</td>
                    <td class="text-left"><input type="checkbox"></td>
                </tr>
                
            </tbody>
        </table>--%>
        
        <%--ASP VERSION--%>
       
      
    <div id="index">
    <div class="Edit">
        <hr class="first_line">
        <asp:Label ID="LabelHomePage" runat="server" class="edit_lable" Text="Home Page Student"></asp:Label>
    </div>
    </div>

        <div id="marks">
        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                <ContentTemplate>
                    <label class="edit_lable">MARKS</label>
                    <hr class="first_line">
                    <div class="Edit">
                    </div>
        
					<asp:GridView ID="courseMarks" runat="server" CssClass="table-fill" AutoGenerateColumns="false">
                        <Columns>
                            <asp:BoundField DataField="CourseName" HeaderText="Course Name"/>
                            <asp:BoundField DataField="CourseType" HeaderText="Course Type"/>
                            <asp:BoundField DataField="Credits" HeaderText="Credits"/>
                            <asp:BoundField DataField="Grade" HeaderText="Grade"/>
                        </Columns>

                        <RowStyle CssClass="tr" />
                        <HeaderStyle CssClass="table-hover" />
                    </asp:GridView>

                </ContentTemplate>
        </asp:UpdatePanel>
    </div>

    
    <div id="courses">
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    <hr class="first_line">
                    <label class="edit_lable">VIEW COURSES</label>
                    <div class="Edit">
                        <asp:Label ID="Label1" runat="server" class="edit_lable" Text="Aici o sa vezi cursurile"></asp:Label>
                        <input id="ButtonCourses" runat="server" type="button" value="COURSES" onserverclick="ButtonCourses_Click" />

                    </div>
        
                    <div class="infobox">
                        <asp:Table ID="Grades" runat="server" CssClass="table-fill">
            <asp:TableHeaderRow CssClass="tr">
                <asp:TableHeaderCell runat="server" CssClass="text-left th" Text="Course"/>
                <asp:TableHeaderCell runat="server" CssClass="text-left th" Text="Type"/>
                <asp:TableHeaderCell runat="server" CssClass="text-left th" Text="Credits"/>
                <asp:TableHeaderCell runat="server" CssClass="text-left th" Text="Mark"/>
                <asp:TableHeaderCell runat="server" CssClass="text-left th" Text="Select"/>
            </asp:TableHeaderRow>
            
            <asp:TableRow runat="server" CssClass="table-hover">
                <asp:TableCell runat="server" CssClass="text-left td" Text="Arhitectura Calculatoarelor"/>
                <asp:TableCell runat="server" CssClass="text-left td" Text="Mendatory"/>
                <asp:TableCell runat="server" CssClass="text-left td" Text="6"/>
                <asp:TableCell runat="server" CssClass="text-left td" Text="10"/>
                <asp:TableCell runat="server" CssClass="text-left td"><input type="checkbox"/></asp:TableCell> <%--Daca nu se poate accesa checkbox-ul foloseste checkboxul din asp. Vezi ToolBox/--%>
            </asp:TableRow>

            <asp:TableRow runat="server" CssClass="table-hover">
                <asp:TableCell runat="server" CssClass="text-left td" Text="Arhitectura Calculatoarelor"/>
                <asp:TableCell runat="server" CssClass="text-left td" Text="Mendatory"/>
                <asp:TableCell runat="server" CssClass="text-left td" Text="6"/>
                <asp:TableCell runat="server" CssClass="text-left td" Text="10"/>
                <asp:TableCell runat="server" CssClass="text-left td"><input type="checkbox"/></asp:TableCell> <%--Daca nu se poate accesa checkbox-ul foloseste checkboxul din asp. Vezi ToolBox/--%>
            </asp:TableRow>
        </asp:Table>
                    </div>
                </ContentTemplate>
        </asp:UpdatePanel>
    </div>
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>   
        <script>
            (function(){
                var body = $('body');
                $('.toggle').bind('click', function(){
                    body.toggleClass('menu-open');
                    return false;
                });
            })();
        </script>

    </form>

</body>

</html>