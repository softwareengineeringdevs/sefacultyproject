﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FacultyManagement.Controllers;
using FacultyManagement.ViewModel;

namespace FacultyManagement.Views
{
    public partial class RecoverPassword : System.Web.UI.Page
    {
        private ViewHomeController ctrl = new ViewHomeController();
		private String ResetCode { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void RecoverButton_Click(object sender, EventArgs e)
        {
			RecoverPasswordViewModel var = new RecoverPasswordViewModel();

			string user = Username_TextBox.Text;
			string email = Email_TextBox.Text;
			UserType type;
			Enum.TryParse(Type_Select.SelectedItem.Text, out type);

			var.Username = user;
			var.Email = email;
			var.TypeOfUser = type;

            Session["New"] = ctrl.RecoverPassword(var);

			if (Session["New"] != null) {
				Username_TextBox.Text = "  A recovery code was sent!";
			}
			else {
				Username_TextBox.Text = "  Wrong data";
			}
        }

        /// <summary>
        /// 
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ChangeButton_Click(object sender, EventArgs e)
        {
            ChangePasswordViewModel var = new ChangePasswordViewModel();

            var.ResetCode = TextBoxResetCode.Text;
            var.NewPassword = TextBoxPassword.Text;
            UserType type;
            Enum.TryParse(Type_Select.SelectedItem.Text, out type);
            var.TypeOfUser = type;
            var.Username = TextBoxUsername.Text;



            if (TextBoxPassword.Text == TextBoxConfirmPassword.Text)
            {
                if (var.ResetCode == Session["New"].ToString())
                {
                    if (ctrl.ChangePassword(var))
                    {
                        Session.Remove("New");
                        Session.RemoveAll();
                        Response.Redirect("Login.aspx");
                    }
                    else
                    {
                        TextBoxResetCode.Text = "Internal issues";
                    }
                }
                else
                {
                    TextBoxResetCode.Text = "Reset codes don't corespond";
                }
                
            }
            else
            {
                TextBoxResetCode.Text = "Passwords don't corespond";
            }
        }
    }
}