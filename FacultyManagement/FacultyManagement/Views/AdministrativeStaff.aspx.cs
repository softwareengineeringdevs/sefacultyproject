﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FacultyManagement.Controllers;
using FacultyManagement.ViewModel;
using FacultyManagement.Models.YearOfStudy;
using System.Windows.Forms;

namespace FacultyManagement.Views
{
    public partial class AdministrativeStaff : System.Web.UI.Page
    {
        private ViewHomeController homeCtrl = new ViewHomeController();
        private ViewAdministrativeStaffController adminCtrl = new ViewAdministrativeStaffController();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["New"] != null)
            {
                string Type = (string)Session["New"];
                Type = Type.Split('.')[0];
                if (Type == "AdministrativeStaff")
                {
                    ;
                }
                else
                {
                    Response.Redirect(Type + ".aspx");
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }

        protected void ButtonAdd_Click(object sender, EventArgs e)
        {
            StudentViewModel var = new StudentViewModel();

            var.Name = StudentAddName.Text;
            var.Group = new StudentsGroup();
            var.Group.Name = StudentAddGroup.Text;
            var.Username = StudentAddUsername.Text;
            var.Password = StudentAddPassword.Text;

            if (StudentAddPassword.Text == StudentAddConfirmPassword.Text && StudentAddPassword.Text != string.Empty)
            {
                if (adminCtrl.Create(var))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "<script>alert('Studentul a fost adaugat');</script>", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "<script>alert('Eroare, daca persista contactati administratorul');</script>", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "<script>alert('Parolele nu corespund');</script>", true);
            }
            StudentAddName.Text = string.Empty;
            StudentAddGroup.Text = string.Empty;
            StudentAddUsername.Text = string.Empty;
            StudentAddPassword.Text = string.Empty;
            StudentAddConfirmPassword.Text = string.Empty;

        }

        protected void ButtonEdit_Click(object sender, EventArgs e)
        {
            StudentViewModel var = new StudentViewModel();

            string username = StudentEditUsername.Text;

            var.Name = StudentEditNewName.Text;
            var.Group = new StudentsGroup();
            var.Group.Name = StudentEditNewGroup.Text;
            var.Username = StudentEditNewUsername.Text;

            if (adminCtrl.Edit(username, var))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "<script>alert('Studentul a fost modificat');</script>", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "<script>alert('Eroare, verifica daca datele sunt valide - daca persista contactati administratorult');</script>", true);
            }

            StudentEditNewName.Text = string.Empty;
            StudentEditNewGroup.Text = string.Empty;
            StudentEditNewUsername.Text = string.Empty;

        }

        protected void ButtonDelete_Click(object sender, EventArgs e)
        {
            if (StudentDeleteUsername.Text == StudentDeleteConfirmUsername.Text)
            {
                if (adminCtrl.Delete(StudentDeleteUsername.Text))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "<script>alert('Userul a fost sters');</script>", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "<script>alert('Userul nu a fost sters, probabil ca nu exista');</script>", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "<script>alert('Idurile introduse nu corespund');</script>", true);
            }

            StudentDeleteUsername.Text = string.Empty;
            StudentDeleteConfirmUsername.Text = string.Empty;
        }

        protected void ButtonDetails_Click(object sender, EventArgs e)
        {
            StudentViewModel var = new StudentViewModel();

            var = adminCtrl.Details(StudentDetailsUsername.Text);

            if (var != null)
            {
                StudentDetailsReadUser.Text = var.Username;
                StudentDetailsReadName.Text = var.Name;
                StudentDetailsReadGroupName.Text = var.Group.Name;

            }
            StudentDetailsUsername.Text = string.Empty;
        }

        protected void ButtonSignout_Click(object sender, EventArgs e)
        {
            Session.Remove("New");
            Session.RemoveAll();
            Response.Redirect("Login.aspx");
        }

        protected void YS_Click(object sender, EventArgs e)
        {
            string YS = TextBoxYearStatistic.Text;
            Dictionary<string, double> listaStudenti = adminCtrl.StudentStatistics(1, YS);

            foreach (var item in listaStudenti.OrderBy(key => key.Value))
            {
                TableRow row = new TableRow();
                TableCell cell1 = new TableCell();
                TableCell cell2 = new TableCell();
                row.Cells.Add(cell1);
                row.Cells.Add(cell2);
                cell1.Text = item.Key;
                cell2.Text = item.Value.ToString();
                GradesY.Rows.Add(row);
            }
        }

        protected void GS_Click(object sender, EventArgs e)
        {
            string GS = TextBoxGroupStatistic.Text;
            Dictionary<string, double> listaStudenti = adminCtrl.StudentStatistics(2, GS);

            foreach (var item in listaStudenti.OrderBy(key => key.Value))
            {
                TableRow row = new TableRow();
                TableCell cell1 = new TableCell();
                TableCell cell2 = new TableCell();
                row.Cells.Add(cell1);
                row.Cells.Add(cell2);
                cell1.Text = item.Key;
                cell2.Text = item.Value.ToString();
                GradesG.Rows.Add(row);
            }
        }
    }
}