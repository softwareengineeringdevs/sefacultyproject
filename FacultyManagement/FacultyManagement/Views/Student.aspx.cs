﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FacultyManagement.ViewModel;
using FacultyManagement.Controllers;
using FacultyManagement.Models.Courses;
using System.Data;

namespace FacultyManagement.Views
{
    public partial class Student : System.Web.UI.Page
    {
        private ViewStudentController ctrl = new ViewStudentController();
        private String username;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Session["New"]!= null)
            {
                string session = (string)Session["New"];
                string type = session.Split('.')[0];
                username = session.Split('.')[1];

				if (type == "Student") {
                    LoadMarks();
				}
				else {
					Response.Redirect(type + ".aspx");
				}
			}
			else {
				Response.Redirect("Login.aspx");
			}
		}

        protected void ButtonCourses_Click(object sender, EventArgs e)
        {
            //aici iau toate cursurile utilizatorului si le afisez
            //in username e userul logat, deci o sa returnez toate cursurile la care e inscris userul username

            //Am comentat chestia asta pana gand o sa o folosesti
            //===================================================

            //string username = (string)Session["New"];
            //username = username.Split('.')[1];



            //          Asa adaugi in tabel
            //===========================================

            for (int i = 0; i < 12; i++)
            {
                CheckBox chkBox = new CheckBox();
                TableRow row = new TableRow();
                TableCell cell1 = new TableCell();
                TableCell cell2 = new TableCell();
                TableCell cell3 = new TableCell();
                TableCell cell4 = new TableCell();
                TableCell cell5 = new TableCell();
                row.Cells.Add(cell1);
                row.Cells.Add(cell2);
                row.Cells.Add(cell3);
                row.Cells.Add(cell4);
                row.Cells.Add(cell5);
                Grades.Rows.Add(row);

                cell1.Text = "OOP";
                cell2.Text = "Mendatory";
                cell3.Text = "6";
                cell4.Text = "10";
                cell5.Controls.Add(chkBox);
            }

            

        }

        public void LoadMarks()
        {
            List<ICourse> list = ctrl.Courses(username);
            if (list.Count == 0)
            {
                //mesaj de eroare, profesorul nu are cursuri
                //return;
            }

            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("CourseName", typeof(string)));
            dt.Columns.Add(new DataColumn("CourseType", typeof(string)));
            dt.Columns.Add(new DataColumn("Credits", typeof(int)));
            dt.Columns.Add(new DataColumn("Grade", typeof(int)));

            foreach (var cgrade in list)
            {
                DataRow dr = dt.NewRow();
                dr["CourseName"] = cgrade.Name;
                dr["CourseType"] = "-";
                dr["Credits"] = cgrade.Credits;
                dr["Grade"] = 0;
                dt.Rows.Add(dr);            
            }
        }

        protected void ButtonSignout_Click(object sender, EventArgs e)
        {
            Session.Remove("New");
            Session.RemoveAll();
            Response.Redirect("Login.aspx");
        }
    }
}