﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FacultyManagement.Models.Courses;
using FacultyManagement.Repositories;
using FacultyManagement.Models.Users;

namespace FacultyManagement.Services {
	public class CheckService {
		private readonly ICourseRepository<MandatoryCourse> mcourses;
		private readonly ICourseRepository<FacultativeCourse> fcourses;
		private readonly ICourseRepository<OptionalCourse> ocourses;
		private readonly IUserRepository<Student> students;

		public CheckService(ICourseRepository<MandatoryCourse> mcourses, ICourseRepository<FacultativeCourse> fcourses, ICourseRepository<OptionalCourse> ocourses, IUserRepository<Student> students) {
			this.mcourses = mcourses;
			this.ocourses = ocourses;
			this.fcourses = fcourses;
			this.students = students;
		}

		public Student CheckStudent(int id) {
			var stud = students.FindById(id);
			if (stud != null) {
				return stud;
			}
			else {
				throw new StudentNotFoundException(id);
			}
		}

		public ICourse CheckCourse(string name) {
			ICourse course=mcourses.FindByName(name);
			if (course != null) {
				return course;
			}
			else {
				course = ocourses.FindByName(name);
				if (course != null) {
					return course;
				}
				else {
					course = fcourses.FindByName(name);
					if (course != null) {
						return course;
					}
					else {
						throw new CourseNotFoundException(name);
					}
				}
			}
		}
	}
}

public class CourseNotFoundException : Exception {
	public CourseNotFoundException(string course)
		: base("The course: " + course + " could not found!") {
	}
}

public class StudentNotFoundException : Exception {
	public StudentNotFoundException(string student)
		: base("The student: " + student + " could not be found!") {
	}
	public StudentNotFoundException(int id)
		: base("The student with id: " + id + " could not be found!") {
	}
}