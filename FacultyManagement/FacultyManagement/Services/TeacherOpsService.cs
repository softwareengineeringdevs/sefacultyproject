﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FacultyManagement.Models.Courses;
using FacultyManagement.Models.Users;
using FacultyManagement.Repositories;
using FacultyManagement.Models;

namespace FacultyManagement.Services {
	public class TeacherOpsService {
		private readonly GradesRepository grades;

		public TeacherOpsService(GradesRepository grades) {
			this.grades = grades;
		}

		public void AddGrade(Grade mark) {
			grades.Add(mark);
		}

	}
}