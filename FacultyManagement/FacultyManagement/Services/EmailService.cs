﻿using System.Net;
using System.Net.Mail;

namespace FacultyManagement.Services
{
	public static class EmailService
	{
		public static void SendEmail(string userEmail, string username, string resetCode)
		{
			var client = new SmtpClient(EmailHost.Smtp, EmailHost.Port)
			{
				Credentials = new NetworkCredential(EmailHost.Email, EmailHost.Password),
				EnableSsl = true
			};
			client.Send(EmailHost.Email, userEmail, EmailHost.Subject, "Username: " + username + "\nReset code: " + resetCode);
		}
	}

	public static class EmailHost
	{
		public static string Email
		{
			get { return "evotest939@gmail.com"; }
		}

		public static string Password
		{
			get { return "evozon systems"; }
		}

		public static string Smtp
		{
			get { return "smtp.gmail.com"; }
		}

		public static int Port
		{
			get { return 587; }
		}

		public static string Subject
		{
			get { return "Recover password"; }
		}
	}
}