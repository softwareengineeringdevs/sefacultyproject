﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using FacultyManagement.Models.Users;
using FacultyManagement.Repositories;
using FacultyManagement.ViewModel;
using FacultyManagement.Models.YearOfStudy;

namespace FacultyManagement.Services
{
	public class StudentService
	{
		private readonly StudentRepository studentRepository;

		public StudentService(StudentRepository studentRepository)
		{
			this.studentRepository = studentRepository;
		}


		public List<StudentViewModel> GetAll()
		{
            var studentEntities = this.studentRepository.GetAll();
			return studentEntities.Select(Mapper.Map<Student, StudentViewModel>).ToList();
		}
        public List<StudentViewModel> GetAllGroup(string group)
        {
            var studentEntities = this.studentRepository.GetAllGroup(group);
            return studentEntities.Select(Mapper.Map<Student, StudentViewModel>).ToList();
        }

        public List<StudentViewModel> GetAllYear(string year)
        {
            var studentEntities = this.studentRepository.GetAllYear(year);
            return studentEntities.Select(Mapper.Map<Student, StudentViewModel>).ToList();
        }

		public StudentViewModel FindById(int id)
		{
			var studentEntity = this.studentRepository.FindById(id);
			return Mapper.Map<Student, StudentViewModel>(studentEntity);
		}

		public void Add(StudentViewModel studentViewModel)
		{
			var studentEntity = Mapper.Map<StudentViewModel, Student>(studentViewModel);
			studentEntity.Password = LoginService.EncodePassword(studentEntity.Password);
			this.studentRepository.Add(studentEntity);
		}

		public void Delete(int id)
		{
			this.studentRepository.Delete(id);
		}

		public void Update(StudentViewModel studentViewModel)
		{

			var studentEntity = Mapper.Map<StudentViewModel, Student>(studentViewModel);
			this.studentRepository.Update(studentEntity);
		}
	}
}