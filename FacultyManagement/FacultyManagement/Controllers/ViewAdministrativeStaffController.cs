﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FacultyManagement.ViewModel;
using FacultyManagement.Services;
using FacultyManagement.Repositories;
using FacultyManagement.Models.Users;
using FacultyManagement.Models.YearOfStudy;
using FacultyManagement.Models.StudentsGrade;
using System.Diagnostics;

namespace FacultyManagement.Controllers
{
    public class ViewAdministrativeStaffController : Controller
    {
        private readonly StudentService studentService = new StudentService(new StudentRepository());
        private readonly StudentsFacultativeCourseGradeRepository facultativeGrades = new StudentsFacultativeCourseGradeRepository();
        private readonly StudentsMandatoryCourseGradeRepository mandatoryGrades = new StudentsMandatoryCourseGradeRepository();
        private readonly StudentsOptionalCourseGradeRepository optionalGrades = new StudentsOptionalCourseGradeRepository();
       

        public StudentViewModel Details(string  username)
        {
            int id = 0;
            try
            {
                id = this.studentService.GetAll().First(student => student.Username.Equals(username)).Id;
            }
            catch { }
            var studentViewModel = this.studentService.FindById(id);
            return studentViewModel;
        }


        public double StudentAverage (int id)
        {
            int[] average = new int[10000];
                            int i = 0;
                            foreach (StudentsFacultativeCourseGrade grade in facultativeGrades.GetAll())
                            {
                                
                                if (grade.StudentId == id)
                                    average[++i] = grade.Grade;
                            }
                            foreach (StudentsMandatoryCourseGrade grade in mandatoryGrades.GetAll())
                            {
                                if (grade.StudentId == id)
                                    average[++i] = grade.Grade;
                            }

                            foreach (StudentsOptionalCourseGrade grade in optionalGrades.GetAll())
                            {
                                if (grade.StudentId == id)
                                    average[++i] = grade.Grade;
                            }
                            return  average.Average();
                         
        }

        public Dictionary<string,double> StudentStatistics (int option, string param2)
        {
            Dictionary<string, double> dictionary = new Dictionary<string, double>();

            switch (option)
            {
                // year
                case 1:
                    {
                        foreach (StudentViewModel student in studentService.GetAllYear(param2))
                        {
                            dictionary.Add(student.Name, StudentAverage(student.Id));
                        }
                        return dictionary;
                    }
                // groups
                case 2:
                    {
                        foreach (StudentViewModel student in studentService.GetAllGroup(param2))
                        {
                            dictionary.Add(student.Name, StudentAverage(student.Id));
                        }
                        return dictionary;
                    }
                default:
                    return null;
            }
        }

        public bool Create(StudentViewModel var)
        {
            if (ModelState.IsValid)
            {
                this.studentService.Add(var);
                return true;
            }
            return false;
        }

        public bool Edit(string username, StudentViewModel var)
        {
            int id = this.studentService.GetAll().First(student => student.Username.Equals(username)).Id;
            var studentViewModel = this.studentService.FindById(id);
            if (studentViewModel == null)
            {
                return false;
            }
            else
            {
                if(ModelState.IsValid)
                {
                    try
                    {

                        var.Id = studentViewModel.Id;
                        var.Password = studentViewModel.Password;

                        this.studentService.Update(var);
                        return true;
                    }
                    catch (StudentGroupNotFoundException exc)
                    {
                        ModelState.AddModelError("groupNotFound", exc.Message);
                        return false;
                    }
                }
            }
            return true;
        }

        public bool Delete(string username)
        {
            int id=0;
            try
            {
                id = this.studentService.GetAll().First(student => student.Username.Equals(username)).Id;
            }
            catch
            {

            }

            var studentViewModel = this.studentService.FindById(id);
            if (studentViewModel == null)
            {
                return false;
            }
            else
            {
                this.studentService.Delete(id);
                return true;
            }
        }

    }
}