﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FacultyManagement.Repositories;
using FacultyManagement.Services;
using FacultyManagement.Models.StudentsGrade;
using FacultyManagement.Models.Users;
using FacultyManagement.Models.Courses;
using FacultyManagement.ViewModel;
using FacultyManagement.Models;

namespace FacultyManagement.Controllers
{
    public class ViewChiefOfDepartmentController : Controller
    {
        private readonly TeacherService teacherService = new TeacherService(new TeacherRepository());
        private readonly StudentsFacultativeCourseGradeRepository facultativeGrades = new StudentsFacultativeCourseGradeRepository();
        private readonly StudentsMandatoryCourseGradeRepository mandatoryGrades = new StudentsMandatoryCourseGradeRepository();
        private readonly StudentsOptionalCourseGradeRepository optionalGrades = new StudentsOptionalCourseGradeRepository();
        private readonly FacultativeCoursesRepository facultativeCourses = new FacultativeCoursesRepository();
        private readonly MandatoryCourseRepository mandatoryCourses = new MandatoryCourseRepository();
        private readonly OptionalCoursesRepository optionalCourses = new OptionalCoursesRepository();


		public List<OptCourseViewModel> GetOptionalCourses() {
			List<OptCourseViewModel> list = new List<OptCourseViewModel>();
			List<OptionalCourse> optCourses =optionalCourses.GetAll();
			OptCourseViewModel model;
			foreach (OptionalCourse opt in optCourses) {
				int crtStuds=optionalGrades.GetAll().Where(x => x.OptionalCourse_Id==opt.Id).Count();
				model = new OptCourseViewModel() {CourseID=opt.Id,CourseName=opt.Name,CurrentStuds=crtStuds,MaxStuds=opt.MaxStud};
				list.Add(model);
			}
			return list;
		}

		public void DeleteOptionalCourse(int id) {
			optionalGrades.RemoveByCourseID(id);
			optionalCourses.Delete(id);
		}

        public double Average(int id)
        {
            double[] average = new double[10000];
            int i = 0;
            List<ICourse> list = new List<ICourse>();
            foreach (FacultativeCourse course in facultativeCourses.GetAll())
                if (course.Teacher_Id == id)
                    average[++i]=facultativeGrades.CourseAverage(course.Id);

            foreach (OptionalCourse course in optionalCourses.GetAll())
                if (course.Teacher_Id == id)
                    average[++i] = optionalGrades.CourseAverage(course.Id);

            foreach (MandatoryCourse course in mandatoryCourses.GetAll())
                if (course.Teacher_Id == id)
                    average[++i] = mandatoryGrades.CourseAverage(course.Id);

            return average.Average();
        }

        public TeacherViewModel TeacherStatistics(int option)
        {

            Dictionary<TeacherViewModel, double> dictionary = new Dictionary<TeacherViewModel, double>();

            foreach(TeacherViewModel teacher in teacherService.GetAll())
                dictionary.Add(teacher,Average(teacher.Id));

            TeacherViewModel best = dictionary.OrderBy(key => key.Value).First().Key;
            TeacherViewModel worst = dictionary.OrderBy(key => key.Value).Last().Key;
         

            switch(option)
            {
                    // teacher with best results
                case 1:
                    {
                        return best;
                    }

                    // teacher with worst results
                case 2:
                    {
                        return worst;
                    }
                default:
                    return null;
            }
            
        }

        public TeacherViewModel Details(string username)
        {
            int id = this.teacherService.GetAll().First(teacher => teacher.Username.Equals(username)).Id;
            var teacherViewModel = this.teacherService.FindById(id);
            return teacherViewModel;
        }

        public bool Create( TeacherViewModel var)
        {
            if (ModelState.IsValid)
            {
                this.teacherService.Add(var);
                return true;
            }
            return false;
        }

        public bool Edit(string username, TeacherViewModel var)
        {
            int id = this.teacherService.GetAll().First(teacher => teacher.Username.Equals(username)).Id;
            var teacherViewModel = this.teacherService.FindById(id);
            if (ModelState.IsValid)
            {
                var.Id = teacherViewModel.Id;
                var.Password = teacherViewModel.Password;

                this.teacherService.UpdateUser(var);
                return true;
            }
            return false;
        }

        public bool Delete(string username)
        {
            int id = this.teacherService.GetAll().First(teacher => teacher.Username.Equals(username)).Id;
            var teacherViewModel = this.teacherService.FindById(id);
            if (teacherViewModel == null)
            {
                return false;
            }
            else
            {
                this.teacherService.DeleteUser(id);
                return true;
            }
        }

    }
}