﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FacultyManagement.ViewModel;
using FacultyManagement.Services;
using FacultyManagement.Repositories;
using FacultyManagement.Models.Courses;
using FacultyManagement.Models.StudentsGrade;
using FacultyManagement.Models.Users;
namespace FacultyManagement.Controllers
{

    public class ViewStudentController : Controller
    {
		private readonly StudentRepository studrepo = new StudentRepository();
        private readonly StudentService studentService = new StudentService(new StudentRepository());
        private readonly IStudentsGradeRepository<StudentsFacultativeCourseGrade> facultativeCoursesRepository = new StudentsFacultativeCourseGradeRepository();
        private readonly IStudentsGradeRepository<StudentsOptionalCourseGrade> optionalCoursesRepository = new StudentsOptionalCourseGradeRepository();
        private readonly IStudentsGradeRepository<StudentsMandatoryCourseGrade> mandatoryCoursesRepository = new StudentsMandatoryCourseGradeRepository();

        public List<ICourse> Courses(string username){
            List<ICourse> var = new List<ICourse>();

            int id = this.studentService.GetAll().First(student => student.Username.Equals(username)).Id;
            var studentViewModel = this.studentService.FindById(id);
            if (studentViewModel == null)
            {

                return var;
            }
            else
            {
                var.AddRange(mandatoryCoursesRepository.FindStudentCourse(id));
                var.AddRange(optionalCoursesRepository.FindStudentCourse(id));
                var.AddRange(facultativeCoursesRepository.FindStudentCourse(id));
                return var;
            }
        }

		public void EnrollStudents(List<ICourse> courses,String username){
			Student stud=studrepo.FindWithoutId(username);
			if (stud == null) {
				throw new UserNotFoundException(username);
			}
			int id = stud.Id;
			foreach(ICourse crs in courses){
				if (crs is MandatoryCourse) {
					StudentsMandatoryCourseGrade grade=new StudentsMandatoryCourseGrade();
					grade.Grade=0;
					grade.MandatoryCourse_Id=crs.Id;
					grade.StudentId=id;
					mandatoryCoursesRepository.Add(grade);
				}
				if (crs is FacultativeCourse) {
					StudentsFacultativeCourseGrade grade = new StudentsFacultativeCourseGrade();
					grade.Grade = 0;
					grade.FacultativeCourse_Id = crs.Id;
					grade.StudentId = id;
					facultativeCoursesRepository.Add(grade);
				}
				if (crs is OptionalCourse) {
					StudentsOptionalCourseGrade grade = new StudentsOptionalCourseGrade();
					grade.Grade = 0;
					grade.OptionalCourse_Id = crs.Id;
					grade.StudentId = id;
					optionalCoursesRepository.Add(grade);
				}
			}
		}
    }
}