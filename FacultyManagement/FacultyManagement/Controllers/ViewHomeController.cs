﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FacultyManagement.ViewModel;
using FacultyManagement.Services;
using FacultyManagement.Repositories;


namespace FacultyManagement.Controllers
{
    public class ViewHomeController : Controller
    {
        private readonly LoginService loginService = new LoginService(
            new AdminStaffRepository(),
            new TeacherRepository(),
            new CODRepository(),
            new StudentRepository()
            );

        public bool Login(LoginViewModel var){
            try{
                return this.loginService.ValidateUser(ref var);
            }
            catch(UserNotFoundException){
                return false;
            }
        }

        public String RecoverPassword(RecoverPasswordViewModel var){
            try{
				return loginService.RecoverPassword(var);
            }
            catch{
                return null;
            }
        }

        public bool ChangePassword(ChangePasswordViewModel var){
            try{
                var user = loginService.GetRepositoryUser(var.Username, var.TypeOfUser);
				loginService.ResetPassword(user, var.NewPassword);
                return true;
            }
            catch{
                return false;
            }
        }
    }
}